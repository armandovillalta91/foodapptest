import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Animation, AnimationController, ViewDidEnter} from '@ionic/angular';

@Component({
  selector: 'app-date-time-picker',
  templateUrl: './date-time-picker.component.html',
  styleUrls: ['./date-time-picker.component.scss'],
})
export class DateTimePickerComponent implements OnInit, AfterViewInit{
  @ViewChild('pickerContainer',{static: false}) picker: ElementRef;
  @Output('pickerState') pickerState = new EventEmitter();
  constructor(private animationCtrl: AnimationController) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    console.log(this.picker);
    const animation: Animation = this.animationCtrl.create()
      .addElement(this.picker.nativeElement)
      .duration(40)
      .fromTo('opacity', '0', '1');
    animation.play();
  }

  async closeAnimation() {
    const animation: Animation = this.animationCtrl.create()
      .addElement(this.picker.nativeElement)
      .duration(40)
      .fromTo('opacity', '1', '0');
    animation.play();
  }

  async closePicker() {
    await this.closeAnimation();
    this.pickerState.emit(false);
  }
}
