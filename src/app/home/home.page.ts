import {Component, ElementRef, ViewChild} from '@angular/core';
import {IonSlides, ViewDidEnter} from "@ionic/angular";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements ViewDidEnter{
  @ViewChild('imageSlider', { static: true }) imageSlider:IonSlides;
  slideOpts = {
    pagination: false
  };
  currentPos = 1;
  private showPicker = false;

  constructor() {
  }

  ionViewDidEnter(): void {
    this.imageSlider.ionSlideDidChange.subscribe(async()=> {
      this.currentPos = await this.imageSlider.getActiveIndex()+1;
    });
  }

  previous() {
    this.imageSlider.slidePrev();
  }

  next() {
    this.imageSlider.slideNext();
  }

  togglePicker() {
    console.log("toogling");
    this.showPicker = !this.showPicker;
  }
}
